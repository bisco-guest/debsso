from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from cryptography.x509.oid import ExtensionOID
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from .models import Certificate
from .ca import CA
import OpenSSL
import os
import shutil
import subprocess
import contextlib
import tempfile

@contextlib.contextmanager
def test_ca():
    try:
        tmpdir = tempfile.mkdtemp()

        ca = CA(cadir=tmpdir)
        ca.init()

        yield ca
    finally:
        #subprocess.check_call(["bash"], cwd=tmpdir)
        shutil.rmtree(tmpdir)


class TestCA(TestCase):
    def assertCorrectClientCertificate(self, cert, userid, ca):
        c = x509.load_der_x509_certificate(cert.der, default_backend())
        self.assertEqual(c.serial_number, cert.serial)
        subject = { attr.oid._name: attr.value for attr in c.subject }
        self.assertEqual(subject, { "commonName": userid, "organizationName": ca.organization })

        extensions = list(c.extensions)
        self.assertEqual(len(extensions), 3)

        #e = c.extensions.get_extension_for_oid(x509.ExtensionOID.SUBJECT_ALTERNATIVE_NAME)
        #self.assertEqual(len(e.value), 1)
        #self.assertEqual(e.value.get_values_for_type(x509.RFC822Name), [userid])
        #self.assertFalse(e.critical)

        e = c.extensions.get_extension_for_oid(x509.ExtensionOID.EXTENDED_KEY_USAGE)
        self.assertEqual(list(e.value), [x509.oid.ExtendedKeyUsageOID.CLIENT_AUTH])
        self.assertFalse(e.critical)

        e = c.extensions.get_extension_for_oid(x509.ExtensionOID.BASIC_CONSTRAINTS)
        self.assertFalse(e.value.ca)
        self.assertEqual(e.value.path_length, None)
        self.assertTrue(e.critical)

        e = c.extensions.get_extension_for_oid(x509.ExtensionOID.KEY_USAGE)
        self.assertTrue(e.value.digital_signature)
        self.assertFalse(e.value.content_commitment)
        self.assertTrue(e.value.key_encipherment)
        self.assertFalse(e.value.data_encipherment)
        self.assertTrue(e.value.key_agreement)
        self.assertFalse(e.value.key_cert_sign)
        self.assertFalse(e.value.crl_sign)
        self.assertFalse(e.value.encipher_only)
        self.assertFalse(e.value.decipher_only)
        self.assertTrue(e.critical)

        r = subprocess.run(["openssl", "x509", "-noout", "-nameopt", "multiline,show_type", "-subject", "-issuer", "-inform", "der"], input=cert.der, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
        self.assertNotIn(b"UTF8STRING", r.stdout)

    def test_create(self):
        with test_ca() as ca:
            self.assertTrue(os.path.isdir(ca.cadir))
            self.assertTrue(os.path.exists(os.path.join(ca.cadir, "index.txt")))
            self.assertTrue(os.path.isdir(os.path.join(ca.cadir, "newcerts")))
            self.assertTrue(os.path.isdir(os.path.join(ca.cadir, "private")))
            self.assertTrue(os.path.exists(os.path.join(ca.cadir, "private/cakey.pem")))
            self.assertTrue(os.path.exists(os.path.join(ca.cadir, "serial")))
            self.assertTrue(os.path.exists(os.path.join(ca.cadir, "cacert.pem")))

    def test_spkac_verify(self):
        user = get_user_model().objects.create_superuser(
                email='test@example.org', password="test", first_name="testfn",
                last_name="testln", provider="test")
        with test_ca() as ca:
            # Create a spkac file
            with tempfile.NamedTemporaryFile(mode="w+t") as tf:
                ca.run_openssl("spkac", "-challenge", "testchallenge", "-key", os.path.join(ca.cadir, "private/cakey.pem"), "-out", tf.name)
                tf.seek(0)
                spkac = tf.read()
                self.assertRegex(spkac, r"^SPKAC=")
                spkac = spkac[6:]

            cert = Certificate(user=user, comment="testcomment", validity=365)
            # Try creating a certifiacate for it
            ca.make_spkac_certificate(spkac, "testchallenge", cert)

            userid = getattr(user, Certificate.get_userid_field_name())
            self.assertCorrectClientCertificate(cert, userid, ca)

    def test_spkac_verify_firefox(self):
        user = get_user_model().objects.create_superuser(
                email='test@example.org', password="test", first_name="testfn",
                last_name="testln", provider="test")
        spkac_challenge = "BCrMgVMY2AstgRwSFnLyGiNz3HfqZhra"
        spkac_firefox = """
MIICYDCCAUgwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDapQuubeWZ
uJm4IYDoBvYMFuMyAVwqPhhPw//tfbQ/8U0FSUxJ6E/RYw55teL1M7CGpvblB79T
bW9tIrKN4G7DCR01Qpo5CVoM05jh5xSXu2ElnSnxDjN42qJXaKXL+dhvQGRwMT/l
XYHTXaE5c6Y4SnBm/cERrR+4cqo5b8/jqUBAYPakxp83ko77l+EF/MZBLd6H81RN
gfezUnwNlJPbxXuROstyWRhW3u6IDT9Ch7EmC7VusQ14y+W7hdv0y32E2hfHa1g1
V2FG0xyrHEGk0gBgADmXk/GwGlwdjOdWF1OKtF1ZvjtJryuTrEJ3I5vvfGrSyssW
mm2PU+Wv0ZeNAgMBAAEWIEJDck1nVk1ZMkFzdGdSd1NGbkx5R2lOejNIZnFaaHJh
MA0GCSqGSIb3DQEBBAUAA4IBAQDSGPE2ftEB6/YYVwBysP/YCYJj/1qh85UoBeJa
Kpdd70SYElNWFsl4i14kTdilGIHnKFuTAYAC6n94l4XT39dqfyduNHlHdnkZA/7M
9B3aY79VrIJuDRr+0wm2gG223InjmqZQt5lcv68maROHk9NbMw4gaWuWzpQYNS/K
XQg/vjrlxF6rNK7TsobWaR82E4UTaGvvYDUTEF4XPgzNK4VlVMyRu8L3F5qjudth
voty6wOjkCMax/7Mdeb4B5tj3yWwCFSa1qySg4HT2DA5qOjgv75HJQKH3hoNU+MD
9c3RPG2tCcoE5Vr+woLttRfRzX2ZkXPrU7Wxm6LtRCA1EuEu""".strip()
        with test_ca() as ca:
            cert = Certificate(user=user, comment="testcomment", validity=365)
            # Try creating a certifiacate for it
            ca.make_spkac_certificate(spkac_firefox, spkac_challenge, cert)

            userid = getattr(user, Certificate.get_userid_field_name())
            self.assertCorrectClientCertificate(cert, userid, ca)

    def test_malicious_csr_verify(self):
        user = get_user_model().objects.create_superuser(
                email='test@example.org', password="test", first_name="testfn",
                last_name="testln", provider="test")

        with test_ca() as ca:
            # Create a malicious csr file
            with tempfile.NamedTemporaryFile(mode="w+t") as tf:
                subject = "/CN=root/O=universe"
                ca.run_openssl("req", "-new", "-sha256", "-subj", subject, "-key", os.path.join(ca.cadir, "private/cakey.pem"), "-batch", "-out", tf.name)
                tf.seek(0)
                malicious_csr = tf.read()
            self.assertIn("-----BEGIN CERTIFICATE REQUEST-----", malicious_csr)

            # Try having the CA sign it, it should refuse to issue one for root
            cert = Certificate(user=user, comment="testcomment", validity=365)
            ca.make_csr_certificate(malicious_csr, cert)

            # We asked for root/universe, but we still get the right certificate
            userid = getattr(user, Certificate.get_userid_field_name())
            self.assertCorrectClientCertificate(cert, userid, ca)

    def test_csr_verify(self):
        user = get_user_model().objects.create_superuser(
                email='test@example.org', password="test", first_name="testfn",
                last_name="testln", provider="test")

        with test_ca() as ca:
            # Create a valid csr file
            with tempfile.NamedTemporaryFile(mode="w+t") as tf:
                userid = getattr(user, Certificate.get_userid_field_name())
                subject = "/CN={}/O={}".format(userid, ca.organization)
                ca.run_openssl("req", "-new", "-sha256", "-subj", subject, "-key", os.path.join(ca.cadir, "private/cakey.pem"), "-batch", "-out", tf.name)
                tf.seek(0)
                csr = tf.read()
            self.assertIn("-----BEGIN CERTIFICATE REQUEST-----", csr)

            cert = Certificate(user=user, comment="testcomment", validity=365)
            # Try creating a certificate for it
            ca.make_csr_certificate(csr, cert)

            self.assertCorrectClientCertificate(cert, userid, ca)


class TestViews(TestCase):
    def test_crl(self):
        response = self.client.get(reverse("ca_crl"))
        self.assertContains(response, "-----BEGIN X509 CRL-----")

    def test_pem(self):
        response = self.client.get(reverse("ca_pem"))
        self.assertContains(response, "-----BEGIN CERTIFICATE-----")


# * Generating a SPKAC to try things out:
#
# openssl spkac -challenge foobar -key /tmp/privpart > /tmp/challenge
# echo "O=Debian SSO client certificate" >> /tmp/challenge
# echo "CN=$USERNAME" >> /tmp/challenge
#
# * Verifying the challenge from a SPKAC:
#
# openssl spkac -in /tmp/challenge -verify
#
# * Generating a certificate from a SPKAC:
#
# openssl ca -config ca.ini -spkac /tmp/challenge
