from django.views.generic import View, TemplateView
from django import http
from django.contrib.auth import get_user_model
from .models import Certificate
from .ca import CA
import os

class ServeCAFile(View):
    # Use octet-stream to avoid a browser accidentally importing the
    # certificate. Export is provided so that people can deploy sites that can
    # verify these certificates, but it is unlikely that a browser will ever
    # need to trust these certificates.
    content_type = "application/octet-stream"
    filename = None

    def get_filename(self):
        ca = CA()
        return os.path.join(ca.cadir, self.filename)

    def get(self, request):
        pathname = self.get_filename()
        with open(pathname, "rb") as infd:
            data = infd.read()
        res = http.HttpResponse(data, content_type=self.content_type)
        res["Content-Disposition"] = 'attachment; filename="{}"'.format(self.filename)
        return res


class CRLView(ServeCAFile):
    content_type = "application/x-pkcs7-crl"
    filename = "ca.crl"


class CACertView(ServeCAFile):
    content_type = "application/octet-stream"
    filename = "cacert.pem"


class TestEnv(TemplateView):
    template_name = "ca/test_env.html"

    def get_context_data(self, **kw):
        ctx = super(TestEnv, self).get_context_data(**kw)
        userid = self.request.META.get("SSL_CLIENT_S_DN_CN", None)
        if userid is not None:
            User = get_user_model()
            query_args = { Certificate.get_userid_field_name(): userid }
            try:
                user = User.objects.get(**query_args)
            except User.DoesNotExist:
                user = None
        else:
            user = None
        ctx["ssl_user"] = user
        ctx["env"] = sorted(x for x in list(self.request.META.items()) if x[0].startswith("SSL_CLIENT_"))
        return ctx


#class InitCA(SPKACMixin, View):
#    def get(self, request):
#        ca = CA()
#        if not os.path.exists(ca.cadir):
#            ca.init()
#        return redirect("spkac_status", domain=self.domain)
#
#
#class UpdateCRL(SPKACMixin, View):
#    def get(self, request):
#        ca = CA()
#        ca.update_crl()
#        return redirect("spkac_status", domain=self.domain)
