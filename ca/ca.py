from django.conf import settings
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
import OpenSSL
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.backends.openssl.backend import Backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from cryptography.x509.oid import NameOID
from .models import Certificate
import lockfile
import os
import re
import shutil
import datetime
import contextlib
import subprocess
import tempfile
import tarfile
import io
import json
import time
import ssl
import logging
from six.moves import shlex_quote

log = logging.getLogger("spkac.ca")

DEFAULT_CA_CONF = """[ ca ]
default_ca      = CA_spkac             # The default ca section

[ CA_spkac ]
dir            = {rootdir}             # top dir
database       = $dir/index.txt        # index file.
new_certs_dir  = $dir/newcerts         # new certs dir

certificate    = $dir/cacert.pem       # The CA cert
serial         = $dir/serial           # serial no file
private_key    = $dir/private/cakey.pem # CA private key
RANDFILE       = $dir/private/randstate # random number file

default_days   = 365                   # how long to certify for
# http://openssl.6102.n7.nabble.com/CRL-amp-default-crl-days-td49987.html
default_crl_days= 3                    # how long before next CRL
default_md     = sha256                # md to use

policy         = policy_any            # default policy
email_in_dn    = no                    # Don't add the email into cert DN

name_opt       = ca_default            # Subject name display option
cert_opt       = ca_default            # Certificate display option
copy_extensions = none                 # Don't copy extensions from request

x509_extensions = usr_cert

# The default value is yes, to be compatible with older (pre 0.9.8) versions of
# OpenSSL.  However, to make CA certificate roll-over easier, it's recommended
# to use the value no
unique_subject = no

[ policy_any ]
# Enforce O from the CA certificate
organizationName       = supplied
#organizationalUnitName = optional
commonName             = supplied

[ usr_cert ]
# Do not generate a CA certificate
basicConstraints = critical,CA:FALSE
# Low-level things that the key can be used for (TODO: verify if keyAgreement is needed)
keyUsage = critical,digitalSignature,keyEncipherment,keyAgreement
# High level things that they key can be used for: in this case, client auth only
extendedKeyUsage = clientAuth
"""

DEFAULT_CA_CERT_CONF = """
# X.509 Certificate options
#
# DN options

# The organization of the subject.
organization = "{organization}"

# Common name
cn = "{common_name}"

# The serial number of the certificate
# Comment the field for a time-based serial number.
serial = 1

# In how many days, counting from today, this certificate will expire.
# Use -1 if there is no expiration date.
expiration_days = -1

# X.509 v3 extensions

# An URL that has CRLs (certificate revocation lists)
# available. Needed in CA certificates.
crl_dist_points = "{crl_url}"

# Whether this is a CA certificate or not
ca

### Other predefined key purpose OIDs

# Whether this key will be used to sign other certificates.
cert_signing_key

# Whether this key will be used to sign CRLs.
crl_signing_key

# Whether this key will be used to sign OCSP data.
#ocsp_signing_key

### end of key purpose OIDs

# Path length contraint. Sets the maximum number of
# certificates that can be used to certify this certificate.
# (i.e. the certificate chain length)
path_len = -1

# OCSP URI
# ocsp_uri = http://my.ocsp.server/ocsp
# CA issuers URI
# ca_issuers_uri = http://my.ca.issuer
"""

@contextlib.contextmanager
def temp_workdir(*args, **kw):
    try:
        tmpdir = tempfile.mkdtemp(*args, **kw)
        yield tmpdir
    finally:
        shutil.rmtree(tmpdir)

@contextlib.contextmanager
def lock_file(pathname):
    lock = lockfile.FileLock(pathname)
    try:
        lock.acquire()
        yield
    finally:
        try:
            lock.release()
        except lockfile.NotLocked:
            pass

class CommandError(Exception):
    """
    Exception thrown in case a command invocation returns an error
    """
    def __init__(self, cmd, out, err, res):
        super(CommandError, self).__init__("{cmd} failed with error {res}: {err}".format(
            cmd=" ".join(shlex_quote(x) for x in cmd),
            res=res,
            err=" ".join(err.split("\n")),
        ))
        self.cmd = cmd
        self.out = out
        self.err = err
        self.res = res


class OpenSSLError(CommandError):
    """
    Exception thrown in case an openssl invocation returns an error
    """
    pass


class CertToolError(CommandError):
    """
    Exception thrown in case a certtool invocation returns an error
    """
    pass


class TraceFile(object):
    """
    File where we save a trace of all operation of a CA session
    """
    def __init__(self, pathname, fd):
        self.pathname = pathname
        self.out = tarfile.open(fileobj=fd, mode="w")

    def add(self, idx, cmdts, cmd, out, err, res):
        self.add_json("{:02}.cmd".format(idx), {
            "argv": cmd,
            "cmdline": " ".join(shlex_quote(x) for x in cmd),
            "result": res,
        }, ts=cmdts)
        self.add_bytes("{:02}.stdout".format(idx), out.encode("utf8"), ts=cmdts)
        self.add_bytes("{:02}.stderr".format(idx), err.encode("utf8"), ts=cmdts)

    def add_bytes(self, name, buf, ts=0):
        tarinfo = tarfile.TarInfo(name=name)
        tarinfo.size = len(buf)
        tarinfo.mtime = ts
        tarinfo.mode = 0o644
        tarinfo.type = tarfile.REGTYPE
        self.out.addfile(tarinfo, io.BytesIO(buf))

    def add_str(self, name, s, ts=0):
        self.add_bytes(name, s.encode("utf-8"), ts=ts)

    def add_json(self, name, obj, ts=0):
        self.add_bytes(name, json.dumps(obj, indent=1).encode("utf-8"), ts=ts)

    def close(self):
        self.out.close()


class TraceCA(object):
    """
    Trace a CA session
    """
    def __init__(self, tracedir):
        self.tracedir = tracedir
        self.commands = []

    def add_command(self, cmd, out, err, res):
        self.commands.append((time.time(), cmd, out, err, res))

    def save(self):
        if not os.path.exists(self.tracedir):
            os.makedirs(self.tracedir, 0o750)
        ts = datetime.datetime.utcnow()
        with tempfile.NamedTemporaryFile(mode="wb", dir=self.tracedir, prefix=ts.strftime("%Y%m%d-%H%M%S-"), suffix=".tar", delete=False) as out:
            os.chmod(out.name, 0o660)
            tracefile = TraceFile(out.name, out)
            try:
                for idx, (cmdts, cmd, out, err, res) in enumerate(self.commands):
                    tracefile.add(idx, cmdts, cmd, out, err, res)
            finally:
                tracefile.close()


def ca_trace(f):
    """
    Decorator that runs self.trace if a function fails
    """
    def wrapped(self, *args, **kw):
        try:
            return f(self, *args, **kw)
        except:
            self.trace.save()
            raise
    return wrapped


class ForcePrintableStringBackend(Backend):
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self._lib.ASN1_STRING_set_default_mask_asc(b"default")
        orig_func = self._lib.X509_NAME_ENTRY_create_by_OBJ
        def wrapper(a, b, c, d, e):
            return orig_func(a, b, self._lib.MBSTRING_ASC, d, e)
        self._lib.X509_NAME_ENTRY_create_by_OBJ = wrapper


class CA(object):
    """
    CA operations for issuing client certificates
    """
    def __init__(self, cadir=None):
        if cadir is None:
            cadir = getattr(settings, "SPKAC_CADIR", os.path.join(settings.DATA_DIR, "spkac_ca"))
        self.cadir = cadir
        self.caconf = os.path.join(self.cadir, "ca.conf")
        self.calock = os.path.join(self.cadir, "lockfile")
        self.organization = getattr(settings, "SPKAC_ORGANIZATION", "Debian SSO client certificate")
        self.trace = TraceCA(os.path.join(self.cadir, "trace"))

        # XXX: this duplcates information in ca.conf, but is required as
        # some of the cryptological operations are moved into memory and
        # run as commands against OpenSSL the library, rather than the CLI.
        #
        # This means that if the location of the private_key or certificate
        # change, some knobs ought to change in here too. Because I don't
        # know how this is used in the wild, I'm going to make some wild
        # assumptions here.
        self.cacert_path = os.path.join(self.cadir, "cacert.pem")
        self.cakey_path = os.path.join(self.cadir, "private/cakey.pem")

    def load_ca_cert(self):
        with open(self.cacert_path, 'rb') as fd:
            return x509.load_pem_x509_certificate(fd.read(), default_backend())

    def load_ca_key(self):
        with open(self.cakey_path, 'rb') as fd:
            return serialization.load_pem_private_key(
                fd.read(), password=None, backend=default_backend())

    @ca_trace
    def init(self):
        """
        Initialise the CA
        """
        # Directory that contains all the CA data files
        self.ensure_path_exists(self.cadir, mode=0o775)

        # CA configuration
        with open(self.caconf, "wt") as conffd:
            print(DEFAULT_CA_CONF.format(rootdir=self.cadir), file=conffd)

        # Private key storage
        self.ensure_path_exists(os.path.join(self.cadir, "private"), mode=0o770)

        # Storage for certs generated by the CA
        self.ensure_path_exists(os.path.join(self.cadir, "newcerts"), mode=0o775)

        # Create empty index.txt
        with open(os.path.join(self.cadir, "index.txt"), "wt"):
            pass

        # Certificate sequence file
        # Start at 0x1000 with the serial number for certificates, to leave the
        # first 0x1000 free for CA certificates
        with open(os.path.join(self.cadir, "serial"), "wt") as serialfd:
            print("0000000000001000", file=serialfd)

        # Generate private certificate
        privkey = os.path.join(self.cadir, "private", "cakey.pem")
        self.run_certtool("-p", "--outfile=" + privkey)
        #self.run_openssl("genrsa", "-out", privkey, "2048")

        # Generate public CA certificate
        pubcert = os.path.join(self.cadir, "cacert.pem")
        pubcert_conf = os.path.join(self.cadir, "cacert.pem.conf")
        with open(pubcert_conf, "wt") as pubcert_conf_fd:
            print(
                DEFAULT_CA_CERT_CONF.format(
                    organization=self.organization,
                    crl_url="https://" + Site.objects.get_current().domain + reverse("ca_crl"),
                    common_name="SSO CA {:%Y-%m-%d}".format(datetime.datetime.utcnow()),
                ), file=pubcert_conf_fd)

        self.run_certtool("--load-privkey=" + privkey,
                          "-s",
                          "--template=" + pubcert_conf,
                          "--outfile=" + pubcert)
        #self.run_openssl("req",
        #                 "-key", privkey,
        #                 "-subj", "/O=Debian SSO client certificate",
        #                 "-set-serial", "1",
        #                 "-new", "-x509",
        #                 "-days", "3650",
        #                 "-extensions", "v3_ca ?????",
        #                 "-out", pubcert)

    @ca_trace
    def make_spkac_certificate(self, spkac, challenge, cert):
        """
        Given a SPKAC string from a browser and a models.Key object, it
        validates the SPKAC against the key challenge, generate a certificate
        and fill the der and generated fields
        """
        # SPKAC content may be split into multiple lines, reduce it to one line
        # only
        spkac = "".join(x.strip() for x in spkac.splitlines())

        # First thing, let openssl verify the signature
        res = subprocess.run(["openssl", "spkac", "-verify"], input=("SPKAC=" + spkac).encode("utf8"), stdout=subprocess.DEVNULL, stderr=subprocess.PIPE)
        res.check_returncode()
        if res.stderr != b"Signature OK\n":
            raise RuntimeError("openssl reports the signature as different than OK: {}".format(repr(res.stderr)))

        # Extract the public key and challenge from the SPKAC package
        # From https://github.com/FFM/pyspkac/blob/master/pyspkac/spkac.py
        from pyasn1.codec.der.decoder import decode as der_decode
        from pyasn1.codec.der.encoder import encode as der_encode
        from pyasn1.error import PyAsn1Error
        from base64 import b64decode, b64encode

        try :
            seq, rest = der_decode(b64decode(spkac))
        except PyAsn1Error as e:
            raise

        if rest:
            raise RuntimeError("ASN.1 decode: data after SPKAC value")
        if len (seq) != 3 or len (seq [0]) != 2 or len (seq [1]) not in (1, 2):
            raise RuntimeError("Unknown SPKAC data format")
        if len (seq [1]) == 2 and seq [1][1]:
            raise RuntimeError("Invalid Public Key Info")

        spki = seq[0][0]
        spkac_challenge = seq[0][1]
        if str(spkac_challenge) != challenge:
            raise RuntimeError("Challenge mismatch")

        from cryptography.hazmat.primitives import serialization
        public_key = serialization.load_der_public_key(der_encode(spki), default_backend())

        self._sign_public_key(cert, public_key)

    @ca_trace
    def make_csr_certificate(self, csr_pem, cert):
        """
        Given a Certificate Signing Request and a models.Key object, it
        generates a certificate from the CSR, and fill the der and generated
        fields
        """
        # Now, we're going to get the user's public key
        csr = x509.load_pem_x509_csr(csr_pem.strip().encode("utf8"), default_backend())
        public_key = csr.public_key()

        self._sign_public_key(cert, public_key)

    def _sign_public_key(self, cert, public_key):
        # Before we go too far, we need the CA Certificate to set the
        # Issuer information, and the CA Private Key material off disk.
        ca_certificate = self.load_ca_cert()
        ca_private_key = self.load_ca_key()

        # Get the user id to set as the x509 Certificate public key and
        # subject.
        userid = getattr(cert.user, Certificate.get_userid_field_name())

        # Now we're going to set not_before and not_after to not before
        # yesterday (sometimes clocks drift a bit too far), and not after
        # the validity time (in days)
        today = datetime.datetime.today()
        not_before = today - datetime.timedelta(1, 0, 0)
        not_after = today + datetime.timedelta(cert.validity, 0, 0)

        # Now, we're going to manually set every attribute of the Certificate
        # except for the Public Key, which we'll read from the CSR.
        builder = x509.CertificateBuilder()
        builder = builder.public_key(public_key)

        builder = builder.subject_name(x509.Name([
            x509.NameAttribute(NameOID.ORGANIZATION_NAME, self.organization),
            x509.NameAttribute(NameOID.COMMON_NAME, userid),
        ]))

        # the Issuer should match our Cert exactly, so let's copy that over.
        builder = builder.issuer_name(ca_certificate.subject)

        builder = builder.not_valid_before(not_before)
        builder = builder.not_valid_after(not_after)

        # Using a radom serial is best practice since predictable serials
        # may lead to predictable material to be signed, which can lead to
        # a number of interesting attacks. By adding entropy, we can avoid
        # some types of attacks.
        while True:
            #serial = x509.random_serial_number()
            import random
            serial = random.randint(10000, 2**31)
            if Certificate.objects.filter(serial=serial).exists():
                continue
            break
        builder = builder.serial_number(serial)

        # We're going to also manually set the user's email in a SAN RFC822Name
        # rather than depending on a semantic understanding of a CA CommonName
        # practice. This can be helpful for existing software that can
        # operate on RFC822Name SANs
        #builder = builder.add_extension(
        #    x509.SubjectAlternativeName(
        #        [x509.RFC822Name(cert.user.email)]
        #    ),
        #    critical=False
        #)

        # We're going to set some BasicConstraints, and make sure that we can't
        # use this as a CA.
        builder = builder.add_extension(
            x509.BasicConstraints(ca=False, path_length=None),
            critical=True,
        )

        builder = builder.add_extension(
            x509.KeyUsage(
                digital_signature=True,
                content_commitment=False,
                key_encipherment=True,
                data_encipherment=False,
                key_agreement=True,
                key_cert_sign=False,
                crl_sign=False,
                encipher_only=False,
                decipher_only=False,
            ),
            critical=True
        )

        builder = builder.add_extension(
            x509.ExtendedKeyUsage([
                x509.oid.ExtendedKeyUsageOID.CLIENT_AUTH
            ]),
            critical=False
        )

        # Now, we'll actually sign and return this Certificate in DER format.
        certificate = builder.sign(
            private_key=ca_private_key, algorithm=hashes.SHA256(),
            backend=ForcePrintableStringBackend()
        )
        cert.der = certificate.public_bytes(encoding=serialization.Encoding.DER)
        cert.serial = serial

        # Mark the cert as generated
        cert.generated = timezone.now()

    @ca_trace
    def revoke(self, cert):
        """
        Revoke a certificate
        """
        cert_pem = ssl.DER_cert_to_PEM_cert(cert.der)
        self.run_openssl("ca", "-config", self.caconf, "-revoke", "-", input=cert_pem)
        self.update_crl()

    @ca_trace
    def update_crl(self):
        crl_file = os.path.join(self.cadir, "ca.crl")
        self.run_openssl("ca", "-config", self.caconf, "-gencrl", "-out", crl_file)

    def ensure_path_exists(self, path, mode=0o777):
        if not os.path.exists(path):
            os.makedirs(path, mode)

    def run_command(self, command, args, exception_type=CommandError, **kw):
        #cmd = ["strace", "-o/tmp/strace.out", command]
        cmd = [command]
        cmd.extend(args)
        log.info("Running command %s", " ".join(shlex_quote(x) for x in cmd))
        kw["env"] = {
            "RANDFILE": os.path.join(self.cadir, "private/randstate"),
        }
        kw.setdefault("universal_newlines", True)
        if "input" not in kw:
            kw.setdefault("stdin", subprocess.PIPE)
        proc = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, **kw)
        self.trace.add_command(cmd, proc.stdout, proc.stderr, proc.returncode)
        if proc.returncode != 0:
            raise exception_type(cmd, proc.stdout, proc.stderr, proc.returncode)
        return proc.stdout, proc.stderr

    def run_openssl(self, *args, **kw):
        with lock_file(self.calock):
            return self.run_command("/usr/bin/openssl", args, exception_type=OpenSSLError, **kw)

    def run_certtool(self, *args):
        return self.run_command("/usr/bin/certtool", args, exception_type=CertToolError)


