from django.conf.urls import url
from . import views

urlpatterns = [
    url(r"^ca.crl", views.CRLView.as_view(), name="ca_crl"),
    url(r"^ca.pem", views.CACertView.as_view(), name="ca_pem"),
    url(r"^test/env$", views.TestEnv.as_view(), name="ca_test_env"),
]
