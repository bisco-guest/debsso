from django.conf.urls import include, url
from django.views.generic import TemplateView, RedirectView
from . import views

from django.contrib import admin
# admin.autodiscover()

test_urlpatters = [
    url(r"inspect", views.inspect_remote_user),
]

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='debsso/index.html'), name="home"),

    url(r'^ca/', include("ca.urls")),
    # Compatibility URLs
    url(r'^spkac/ca.crl', RedirectView.as_view(pattern_name='ca_crl', permanent=True)),
    url(r'^spkac/ca.pem', RedirectView.as_view(pattern_name='ca_pem', permanent=True)),

    url(r'^(?P<domain>debian|alioth|salsa)/certs/', include('spkac.urls')),

    url(r'^debian/admin/', include(admin.site.urls)),
    url(r'^license/$', TemplateView.as_view(template_name='license.html'), name="root_license"),
]
