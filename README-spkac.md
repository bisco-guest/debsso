# Deploying django-spkac

> I'd love to play with this in a self-host type of set-up. I looked at the
> references - but couldn't find much on how one might set up a similar service
> for "sso.example.com".

With a little Django knowledge it should be easy to set up a sso.example.com
site, but I haven't yet managed to publish instructions and code properly.
Perhaps you could help?

The code is [here](http://anonscm.debian.org/cgit/debian-sso/debian-sso.git/tree/)
and you really only need the "ca" and "spkac" Django apps.

To set up a sso.example.com:

 1. you bootstrap a new Django project
 2. you add the spkac app
 3. you add `SPKAC_USERID_FIELD = "username"` to `settings.py` (or pick
    another use field if you want the certificates to identify users by
    some other property: the default is the database ID)
 3. you run `./manage.py ca_init`
    and you'll get all the CA machinery configured in `data/spkac_ca`
 4. you add spkac's URLs to the site `urls.py`
 5. you schedule a cron job to run `./manage.py ca_update_crl` once or
    twice a day

That is it, really. The app is entirely generic and should plug easily
in any django site: this should make it *extremely* reusable.

I'd like to turn that into a properly packaged Django app, maybe with
code on GitHub, as Django apps usually do, and listed on
https://www.djangopackages.com/categories/apps/
